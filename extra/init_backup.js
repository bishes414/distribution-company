var pg = require('pg');

var ws = require("nodejs-websocket");

var websocketPort = 3000;


var d = new Date();
var isoDate = d.toISOString(d);
// console.log(isoDate);
var substr = isoDate.substring(0,10);


var queryVans = [];
var queryTasks = [];
var queryClients = [];
var queryNames = [];
var queryLat = [];
var queryLong = [];
var queryDate = [];


var customers = [];
var latitude = [];
var longitude = []; 

var tasks = [];
var vehicles = []; 

var custid = [];
var customernames = [];
var address = [];
var time = [];
var days = [];


var wsServer = ws.createServer(function (conn) {
    conn.on("text", function (message) {

      
      console.log(message);
      message = JSON.parse(message);   

      // console.log(message.origin);  // accessible here  

        if(message.origin == 'dropdown'){
          // Fetch data for the list 
          // console.log(vehicles);   
          var jList = {
            "table": "list", 
            "data": vehicles
          }
          conn.sendText(JSON.stringify(jList));
          // transfer(jList);      
        }  
        
        if(message.origin == 'client'){
          // insert into customer
        
          name = message.data.name;
          address = message.data.address; 
          phone = message.data.phone;
          latitude = message.data.latitude;
          longitude = message.data.longitude;   

          insertClient(name, address, phone, latitude, longitude);  

          // console.log(message.data.name);
          //variables accessible here 

        }

        else if(message.origin == 'vehicle'){
          // insert into vehicle
          vehicleid = message.data.vehicleid; 
          drivername = message.data.drivername;
          status = message.data.status;
          // console.log(vehicleid + drivername + status);

          insertVehicle(vehicleid, drivername, status);
        }
        else if(message.origin == 'job'){
          // insert into tasks 
          custid = message.data.custid;
          product = message.data.product;
          quantity = message.data.quantity;
          date = message.data.date;
          time = message.data.time;
          vehicleid = message.data.vehicleid;

          // console.log(custid+product+quantity+date+time+vehicleid);
          insertJob(custid, product, quantity, date, time, vehicleid);
        }
        else if(message.origin == 'ask4data'){
          data = message.vandata;
          // console.log(data);
          getTasksForVan(data);
        }
        else if (message.origin == 'map'){
          // conn.sendText(jCustomers);       
        }


// updated arrays accessible here

  });
  jCustomers = {
    "table": "customers",
    "data": customers,
    "id": custid,
    "lat": latitude,
    "long": longitude
  }
  jTasks = {
    "table": "tasks",
    "data": tasks,
    "names": customernames,
    "address": address,
    "time": time,
    "days": days //date
  }
  jVehicles = {
    "table": "vehicles",
    "data": vehicles
    // "date": date
  }
  jmapData = {
    "table": "map",
    "lat": latitude,
    "long": longitude
  }



  jCustomers = JSON.stringify(jCustomers);
  jTasks = JSON.stringify(jTasks);
  jVehicles = JSON.stringify(jVehicles);
  jmapData = JSON.stringify(jmapData);

  // console.log(jCustomers);
  // console.log(jTasks);
  // console.log(jVehicles);

  conn.sendText(jCustomers);
  conn.sendText(jTasks);
  conn.sendText(jVehicles);  
  conn.sendText(jmapData);

}); 
wsServer.listen(websocketPort);




function transfer(msg) {
    wsServer.connections.forEach(function (conn) {
    // console.log("HERE" + typeof(wsServer.connections));
    // wsServer.getConnections(function(conn){
 

        console.log('Sent to dashboard: ' + JSON.stringify(msg));
    conn.sendText(JSON.stringify(msg));
    });
}







var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });



var connection = "postgres://bishes414:bishes@localhost/distribution";
var newconnection = "postgres://bishes414:bishes@localhost/map"; 

var client = new pg.Client(connection);

var name, address, phone, latitude, longitude;

app.use(express.static(__dirname+'/public/'));
// app.use(express.static(__dirname));

app.get('public/dashboard.html', function(req, res) {
    res.sendFile( __dirname + "/public" + "/dashboard.html" );
});



// function fetchVehicle(){
//   var fetch = new pg.Client(connection);

//   fetch.connect(function(err) {
//   if(err) {
//     return console.error('Unable to connect.', err);
//   }


//   fetch.query("SELECT * FROM vehicle", function(err, result){
    

//       if(err) {
//       console.log('Failed to run query!');
//       }
//       if(result.rows.length){
//         console.log('Success'); 
//       }

//     for(i = 0; i < result.rows.length; i++){
//       vehicleID = result.rows[i].vehicleid;
//       vehicles.push(vehicleID);
//     }
//     for(i = 0; i < result.rows.length; i++){
//       taskname = result.rows[i].taskid;
//       tasks.push(taskname);
//     }

//     console.log('List of vehicles: ' + vehicles);
//       // vehicles = vehicles.join(',');

//     // transfer(vehicles);
//     fetch.end();  
//     });
// });

// }


function insertClient(name, address, phone, latitude, longitude){

var addclient = new pg.Client(connection);

  addclient.connect(function(err) {
          if(err) {
            console.error('Unable to connect.');
          }
      
        addclient.query("INSERT INTO customer (name, address, phone, latitude, longitude) \
            VALUES ($1,$2,$3,$4,$5)",[name, address, phone, latitude, longitude], function(err){
            
            // console.log('Function envoked!!');

              if(err) {
              console.log('Failed to run query!');
            }
             else{
            console.log('Success!');
            }
            addclient.end();  
            });

        });
}


function insertVehicle(vehicleid, drivername, status){

var addvehicle = new pg.Client(connection);

 addvehicle.connect(function(err) {
          if(err) {
            console.error('Unable to connect.');
          }
      
        addvehicle.query("INSERT INTO vehicle (vehicleid, drivername, status) \
            VALUES ($1,$2,$3)",[vehicleid, drivername, status], function(err){
            
            // console.log('Function envoked!!');

              if(err) {
              return console.error('error running query', err);
              }
             else{
            console.log('Success!');
            }
            addvehicle.end();  
            });

        });   
}

function insertJob(custid, product, quantity, date, time, vehicle){

  var addjob = new pg.Client(connection);

  addjob.connect(function(err) {
          if(err) {
            console.error('Unable to connect.');
          }
      
        addjob.query("INSERT INTO tasks (custid, product, quantity, date, time, vehicle) \
            VALUES ($1,$2,$3,$4,$5, $6)",[custid, product, quantity, date, time, vehicle], function(err){
            
            // console.log('Function envoked!!');

              if(err) {
              return console.error('error running query', err);

            }
             else{
            console.log('Success!');
            }
            addjob.end();  
            });
      });

  }












// Query to read database tables and fetch rows

var vanSel = new pg.Client(connection);

  vanSel.connect(function(err) {
  if(err) {
    return console.error('Unable to connect.', err);
  }

// SELECT * FROM tasks INNER JOIN customer ON (customer.id = tasks.custid)

  vanSel.query("SELECT * FROM vehicle", function(err, result){
    // INNER JOIN tasks ON (tasks.vehicle = vehicle.vehicleid)

      if(err) {
      console.log('Failed to run query!');
      }
      if(result.rows.length){
        // console.log('Success'); 
      }

    for(i = 0; i < result.rows.length; i++){
      vehicleID = result.rows[i].vehicleid;
      vehicles.push(vehicleID);
      // job = result.rows[i].taskid;
      // dailyjobs.push(job);
      // dte = result.rows[i].date;
      // date.push(dte);
    }

    console.log('List of vehicles: ' + vehicles);
    // console.log('Jobs: ' + dailyjobs);
      // vehicles = vehicles.join(',');

    // transfer(vehicles);
    vanSel.end();  
    });



});



var vanQuery = new pg.Client(connection);
vanQuery.connect(function(err) {
  if(err) {
    return console.error('Unable to connect.', err);
  }

// SELECT * FROM tasks INNER JOIN customer ON (customer.id = tasks.custid)
// tasks and customers for only today for vehicle 
  // vanQuery.query("SELECT taskid, custid FROM tasks WHERE vehicle = $1 AND date = $2", [queryVan, substr], function(err, result){
    // INNER JOIN tasks ON (tasks.vehicle = vehicle.vehicleid)
  // all tasks and customers for all vehicles 
  vanQuery.query("SELECT date, taskid, custid, vehicle FROM tasks", function(err, result){


      if(err) {
      console.log('Failed to run query!');
      }
      if(result.rows.length){
        // console.log('Success'); 
      }

    for(i = 0; i < result.rows.length; i++){
      qtasks = result.rows[i].taskid;
      queryTasks.push(qtasks);
      qClients = result.rows[i].custid;
      queryClients.push(qClients); 
      qDate = result.rows[i].date;
      queryDate.push(qDate);
      qVans = result.rows[i].vehicle;
      queryVans.push(qVans);
      // job = result.rows[i].taskid;
      // dailyjobs.push(job);
      // dte = result.rows[i].date;
      // date.push(dte);
    }

    console.log('Tasks for BA1JA 1230 are ' + queryTasks + ' for Customers ' + queryClients);
    // function to get customer name and location from query

    for(i = 0; i < queryClients.length; i++){  // loop through all customers 
      selectDetails(queryClients[i], queryVans[i], queryDate[i]);
    }
    // myfunction(); // run insert queries 

    vanQuery.end();  
    });  
});




function selectDetails(queryClients, vans, date){
  var details = new pg.Client(connection);

  

    details.connect(function(err) {
    if(err) {
      return console.error('Unable to connect.', err);
    }
  
    details.query("SELECT name, latitude, longitude FROM customer WHERE id = $1",[queryClients], function(err, result){
      // Select the tasks with specific vehicle for the current day 

        if(err) {
        console.log('Failed to run query!');
        }
        if(result.rows.length){
          console.log('Success '); 
        }

      

      for(i = 0; i < result.rows.length; i++){
        names = result.rows[i].name;
        // queryNames.push(names); // Populate the tasks in empty array 
        lats = result.rows[i].latitude;
        // queryLat.push(lats);
        longs = result.rows[i].longitude;
        // queryLong.push(longs);
      }


      console.log('Name: ' + names + ' Lat: ' + lats + ' Long: ' + longs + ' Date: ' + date);

      // Function to insert into the table of new database 
      insertDetails(names, lats, longs, date, vans);
      
      
        
      details.end();  
      });
   

// loop of queries for each clients of one vehicle tasks 

    
  });

}



// console.log(' Outside, Name: ' + queryNames + ' Lat: ' + queryLat + ' Long: ' + queryLong);


function insertDetails(names, lats, longs, dates, vans){
  var insert = new pg.Client(newconnection);

  insert.connect(function(err) {
          if(err) {
            console.error('Unable to connect.');
          }
      
        insert.query("INSERT INTO vandata (client, latitude, longitude, date, vehicle) \
            VALUES ($1,$2,$3,$4,$5)",[names, lats, longs, dates, vans], function(err){
            
            // console.log('Function envoked!!');

              if(err) {
              return console.error('error running query', err);

            }
             else{
            console.log('Success!');
            }
            insert.end();  
            });
      });  
}




function getTasksForVan(data){
    var taskSel = new pg.Client(connection);

    taskSel.connect(function(err) {
    if(err) {
      return console.error('Unable to connect.', err);
    }
    console.log(data);


    taskSel.query("SELECT taskid FROM tasks WHERE vehicle = $1 AND date = $2",[data, substr], function(err, result){
      // Select the tasks with specific vehicle for the current day 

        if(err) {
        console.log('Failed to run query!');
        }
        if(result.rows.length){
          console.log('Success '); 
        }

      var dailyjobs = [];
      var date = [];

      for(i = 0; i < result.rows.length; i++){
        job = result.rows[i].taskid;
        dailyjobs.push(job); // Populate the tasks in empty array 
        dte = result.rows[i].date;
        date.push(dte);
      }
        jvanData = {
    "table": "vantasks",
    "data": dailyjobs
    }


      console.log('Tasks: ' + dailyjobs);
        // vehicles = vehicles.join(',');

      transfer(jvanData);
      taskSel.end();  
      });
  });

}









  client.connect(function(err) {
  if(err) {
    return console.error('Unable to connect.', err);
  }


  client.query("SELECT * FROM customer", function(err, result){
      

      if(err) {
      console.log('Failed to run query!');
      }
      if(result.rows.length){
        // console.log('Success'); 
      }

      for(i = 0; i < result.rows.length; i++){
      customername = result.rows[i].name;
      id = result.rows[i].id;
      lat = result.rows[i].latitude;
      long = result.rows[i].longitude;
      
      custid.push(id);
      customers.push(customername);
      latitude.push(lat);
      longitude.push(long);
      }

      console.log('List of customers: ' + customers);
        // customers = customers.join(',');

        // transfer(customers);
      

    client.end();  
    });



  client.query("SELECT * FROM tasks INNER JOIN customer ON (customer.id = tasks.custid)", function(err, result){

    

      if(err) {
      console.log('Failed to run query!');
      }
      if(result.rows.length){
        // console.log('Success'); 
      }

    for(i = 0; i < result.rows.length; i++){
      taskname = result.rows[i].taskid;
      tasks.push(taskname);
      custname = result.rows[i].name;
      customernames.push(custname);
      addr = result.rows[i].address;
      address.push(addr);
      day = result.rows[i].date;
      days.push(day);
      tme = result.rows[i].time;
      time.push(tme);
    }
    // transfer(tasks);
    console.log('List of tasks: ' + tasks);
      // tasks = tasks.join(',');

    client.end();  
    });

  });






var expServer = app.listen(8081, function () {
  var host = expServer.address().address;
  var port = expServer.address().port;

  console.log('Listening at http://%s:%s', host, port);
});
