    "use strict"

var currentVan = []; // For getting current ID of vehicle to update innerHTML
var i; 

var is_clicked = 0;

var d = new Date();
var isoDate = d.toISOString(d);
var substr = isoDate.substring(0,10);


var is_logged; 





// Websocket start

    
    var ws = new WebSocket("ws://127.0.0.1:3000");
function sendData(data){

    var jsonData = {
            "origin": "ask4data", 
            "vandata": data
          };

    waitForSocketConnection(ws, function() { 
        ws.send(JSON.stringify(jsonData));

    });

    
}

function sendCat(data, catId){
    var jsonCat = {
        "origin": "requestCat", 
        "catdata": data,
        "catID": catId
    }
    waitForSocketConnection(ws, function() {
        ws.send(JSON.stringify(jsonCat));
    });
}

function bodyLoaded(){
    var jsonData = {
        "origin": "body", 
        "status": "loaded"
    }
    waitForSocketConnection(ws, function() {
        ws.send(JSON.stringify(jsonData));

    });
    

}



function sendExpData(data){


// sendData(name, address, phone, latitude, longitude);
    var jsonData = {
            "origin": "expData", 
            "vandata": data
          };

    waitForSocketConnection(ws, function() { 
        ws.send(JSON.stringify(jsonData));

    });
}


// Socket.io

var socket=io();

    // var demo = document.getElementById('demo');
    socket.on('server', function(data){
        //socket.emit('tab2', data);
        //for(dat in data){
            // demo.innerHTML = data.message;
            console.log(data.message);
        //}

        socket.emit('client','Hello from client');

    });



// ......


var location_list = ['Near Arm Futsal', 'Dilli Bazar', 'Anamnagar', 'Putalisadak'];
var issuedDate = ['2015-11-12', '2015-11-16', '2015-11-18', '2015-11-20'];
// var status = ['done', 'done', 'pending', 'pending'];
var stat = [0, 0, 1, 1];


var parsedData = [['Near Arm Futsal', '2015-11-12', 0], ['Dilli Bazar', '2015-11-16', 0], 
    ['Putalisadak', '2015-11-20', 1]]; 



var activity_list = ['Collect cash from futsal Kirana', 'Deposit 10 gallons of water', 
'Deposit 10 cartoons of chow chow', 'Meet Ram Prasad and talk about cash', 'Take note of new orders'];



var actTable = document.getElementById('vehicle_activities_table');
for(i = 0; i < location_list.length; i++){
    var statStr; 
    if(stat[i] == 0){
        statStr = 'Done';
    }
    else {
        statStr = 'Pending'; 
    }
    var newTableRow = document.createElement('tr');
    newTableRow.setAttribute('id', 'tr' + (i+1));
    newTableRow.classList.add("tableRows");
    var newTableCol = document.createElement('td');
    newTableCol.setAttribute()
    newTableRow.innerHTML = '<td>' + location_list[i] + '</td><td>' + issuedDate[i] + '</td><td>' + statStr + '</td>'; 
    actTable.appendChild(newTableRow);
}
var lent = document.getElementsByClassName('tableRows');
for(i = 0; i < lent.length; i++){
    lent[i].onclick = function (){
        alert(this.innerHTML);
    }
}
//this is nothing


var actList = document.getElementById('activity_list');
for(i = 0; i < activity_list.length; i++){
    var newAct = document.createElement('input');
    var radioSpan = document.createElement('span');
    newAct.setAttribute('type', 'checkbox');
    newAct.classList.add('radioClass');
    radioSpan.classList.add('spanClass');
    // console.log(activity_list[i]);
    newAct.setAttribute('value', activity_list[i]); 
    newAct.setAttribute('id', 'radio' + (i + 1)); 
    // desc.innerHTML = activity_list[i];
    actList.appendChild(newAct);
    // actList.appendChild(desc); 
    // var radioSpan = document.createElement('span');
    radioSpan.innerHTML =  newAct.value + '<br />';
    actList.appendChild(radioSpan);
}
var abc = document.getElementsByClassName('radioClass');
for(i = 0; i < abc.length; i++){
    abc[i].onclick = function(){
        // console.log('Checked!');
    }
}



ws.onmessage = function (evt){
             if(evt.data) {
                var message = evt.data;
                // console.log(message);
                message = JSON.parse(message);
                // console.log(message.table);


        //      27.703262, 85.311824 NEWROAD 
        //      27.726665, 85.345727 SUKEDHARA 
        //      27.671104, 85.324563 PATAN
        //      27.701418, 85.320382 FUN PARK 

                if(message.table == 'session'){
        // console.log(message.is_logged);
        is_logged = message.is_logged;
        if(is_logged != 1){
        }
       }
    
    if(message.table == 'customers'){
        //update customers div

        var listClients = document.getElementById('listclients');
        if(message.data){
            for(i = 0; i < message.data.length; i++){
                var listitem = document.createElement("li");        

            listitem.innerHTML = 'Customer ID <b>' + message.id[i] + '</b>,  Name: <b>' + message.data[i] + 
            '</b>, Location: <b>' + message.lat[i] + '</b>, <b>' + message.long[i] + '</b>'; 
            listClients.appendChild(listitem);
            }    
        }

               
        
    }

    

    if(message.table == 'tasks'){
        //update tasks div
            var listJobs = document.getElementById('listjobs');
        for(i = 0; i < message.data.length; i++){
            // console.log(substr);
            // console.log(message.days[i])
            // tasks for today 
            if(message.days[i] == substr){
            // console.log('matched!');   
            var listitem = document.createElement("li");        

            listitem.innerHTML = 'Task No. <b>' + message.data[i] + '</b> to <b>' + message.names[i] + 
            '</b> at location <b>' + message.address[i] + '</b> today at <b>' + message.time[i] + '</b>.  <u><b>' + message.stat[i].toUpperCase() + '</b></u>';
            listJobs.appendChild(listitem);
 
            }
            // other days 
        //     else{
        //     var jobs = document.getElementById('jobs');
        //         jobs.innerHTML = jobs.innerHTML + 'Task Number ' + message.data[i] + ' to ' + message.names[i] + 
        //         ' at location ' + message.address[i] + ' on ' + message.days[i] + ' at ' + message.time[i];
        //         jobs.innerHTML = jobs.innerHTML + '<br>';
        //     }
        // // console.log(message.days[i]);


        
        }
    }


    if(message.table == 'category'){
        //update vehicles div
        // console.log(message);
        var listCats = document.getElementById('all_categories');
        // console.log(listCats);
        if(message.data){
            for(i = 0; i < message.data.length; i++){
                // Display the jobs assigned to vehicles today
                var item = document.createElement("div"); // Creates a list element 
                var newline = document.createElement('br');
                item.style.width = "150px";
                // item.style.background = "#F0F8FF";
                item.classList.add("catClass");
                item.classList.add("btn-default");

                // listitem.setAttribute("class", 'vans'+(i+1));
                item.setAttribute("id", 'cat'+(i+1));   // Gives ID to the list element 
                // newline.setAttribute("id", 'fill'+(i+1));

                item.innerHTML = message.data[i].toUpperCase();
                // newline.innerHTML = '\n';

                listCats.appendChild(item);
                // listCats.innerHTML = listCats.innerHTML + item.innerHTML + '<br>';
                listCats.appendChild(newline);

            }    
        }
        var catID = [];
        var catClass = document.getElementsByClassName('catClass');
       
            // console.log(listClass.length);
       
            // var togg = document.getElementById('all_categories');

        for(i = 0; i < catClass.length; i++){

            catID[i] = document.getElementById('cat'+(i+1));

        catID[i].onclick = function(){
            // var newDiv = document.createElement('div');

            var data = this.innerHTML.toLowerCase();
            var catId = this.id;
            // Toggle: hide/show 

            // currentCat = this.id; // Gets the ID of this element

            sendCat(data, catId);
            // alert(data); 
            
            }    
        }

    }


    if(message.table == 'vehicles'){
        //update vehicles div
        // console.log(message);
        var listVans = document.getElementById('all_vehicles');
        if(message.data){
            for(i = 0; i < message.data.length; i++){
                // Display the jobs assigned to vehicles today
                var listitem = document.createElement("li"); // Creates a list element 
                // var newline = document.createElement('p');
                listitem.classList.add("vehicleClass");
                // listitem.classList.add("btn-primary");

                // listitem.setAttribute("class", 'vans'+(i+1));
                listitem.setAttribute("id", 'vehicle'+(i+1));   // Gives ID to the list element 
                // newline.setAttribute("id", 'fill'+(i+1));

                listitem.innerHTML = message.data[i];
                // newline.innerHTML = '\n';

                listVans.appendChild(listitem);
                // listvans.appendChild(newline);

                // }

                // Other days 
                // else{
                //     var vans = document.getElementById('vans');
                // vans.innerHTML = vans.innerHTML + 'Vehicle Identifier: ' + message.data[i] + 
                // ', Task No. ' + message.tasks[i] + ' on ' + message.date[i];
                // vans.innerHTML = vans.innerHTML + '<br>';
                // }

            }    
        }
        

        
        var listID = [];
        var listClass = document.getElementsByClassName('vehicleClass');
       
            // console.log(listClass.length);
       

        for(i = 0; i < listClass.length; i++){

            listID[i] = document.getElementById('vehicle'+(i+1));
        listID[i].onclick = function(){
            var data = this.innerHTML;
            currentVan = this.id; // Gets the ID of this element

            var selected = document.getElementsByClassName('currVehicle');
            for(i = 0; i < selected.length; i++){
                selected[i].classList.remove('currVehicle');
            }
            this.classList.add('currVehicle');

            
            sendData(data); 
            // alert(data);
            
            
            
            }    
        }

    }

                

                if(message.table == 'vantasks'){
                  if(message.latitude){
                    var togoLat = message.latitude;
                    var togoLong = message.longitude; 
                    // alert(togoLat + ' + ' + togoLong); // All values are fetched correctly :) 
                    // console.log('To deliver to: ');
                    // console.log('My array: Lat- ' + togoLat + ', Long- ' + togoLong); // _/ Fine 

                    
                    var rad = [150,480,300,280,450,470];
                    // console.log('No. of items: ' + togoLat.length);
                    var waypoints = [];
                    var polyline_options = {
                        color: '#000'
                    };
                    for(var i = 0; i < togoLat.length; i++){
                        // console.log('My array: Lat-> ' + togoLat + ', Long-> ' + togoLong); // _/ Fine 
                      // console.log(typeof(togoLat));
                      stoppage(togoLat[i], togoLong[i], rad[i]);
                      addToDeliverMarker(togoLat[i], togoLong[i]);
                      var latlist = [27.703262, 27.726665, 27.671104, 27.701418, 27.68139]; 
                      var longlist = [85.311824, 85.345727, 85.324563, 85.320382, 85.30197];
                      // console.log(typeof(latlist));


                      var waypts = L.latLng(togoLat[i], togoLong[i]); //L.LatLng()
                      // console.log(waypts);
                      waypoints.push(waypts);

                      // console.log(togoLat[i] + ', ' + togoLong[i]);
                    }
                    
                    // console.log('locations to plot with polylines: ' + waypoints);
                      // console.log(waypoints);

                        drawRoute(waypoints, polyline_options);

                      // drawRoute(waypoints);

                  }
                }

                if(message.table == 'catVehicles'){
                    var ID = message.id;
                    var expandList = document.getElementById(ID);
                    // expandList.innerHTML = ''; 
                    // console.log(message.category);

                    // console.log(message.data);
                    var category = message.category;

                    if(message.data){
                        for(i = 0; i < message.data.length; i++){
                            var expand = document.createElement("li"); // Creates a list element 
                            expand.classList.add(category+"Class"); 
                            expand.classList.add('hoverList');
                            // Assign class to the vehicle relative to category name 
                            expand.setAttribute("id", category+(i+1));   
                            // Assign id to vehicle relative to category name 
                            expand.innerHTML = message.data[i];
                            // expandList.appendChild(expand);
                            // $(expandList).append(expand);
                            // var newTag = document.createElement('ul');
                            // newDiv.appendChild(expand);
                            expandList.appendChild(expand);

                        }   
                    }
                    var expID = [];
        var expClass = document.getElementsByClassName(category+'Class');

       // Counting the elements of class 

            // console.log(listClass.length);
       

        for(i = 0; i < expClass.length; i++){

            expID[i] = document.getElementById(category+(i+1));
            // Getting vehicle by asigned ID 
        expID[i].onclick = function(){
            var data = this.innerHTML;
            // currentCat = this.id; // Gets the ID of this element
            var active = document.getElementsByClassName('currVehicle');
            for(i = 0; i < active.length; i++){
                active[i].classList.remove('currVehicle');    
            }
            this.classList.add('currVehicle');

            sendExpData(data);
            // alert(data); 
            
            
            
            }    
        }

                
                }

                
             }
        }

var waitTimeOut = 0;

        function waitForSocketConnection(socket, callback) {

            setTimeout(
                function() {
                    waitTimeOut = waitTimeOut + 1;
                    if (waitTimeOut === parseInt(3)) {
                        waitTimeOut = 0;
                        return;
                    }

                    if (socket.readyState === 1) {
                        // console.log("Connection is made");
                        if (callback != null) {
                            callback();
                        }
                        return;
                    } else {
                        console.log("wait for connection...")
                        waitForSocketConnection(socket, callback);
                    }

                }, 1000); // wait 1 second for the connection...
        }

    
// Websocket End
    



    // var expand = document.getElementById('expand');

    // if(message.table == 'vantasks'){
            
            
    //          //Disable updating on clicking twice
    //             var vanid = document.getElementById(currentVan);
    //     if(message.data.length > 0){
    //         // vanid.innerHTML = vanid.innerHTML + '<br>  > Task No. ' + message.data;
    //         // alert(message.data);
    //         expand.innerHTML = '<span class = "glyphicon glyphicon-console"></span> Vehicle: ' + message.vehicle + '<br> Tasks: ' + message.data;
    //     }
    //     else{
    //         // vanid.innerHTML = vanid.innerHTML + '<br>  > No tasks listed.'
    //         // alert(message.data);
    //         expand.innerHTML = '<span class = "glyphicon glyphicon-console"></span>Vehicle: ' + message.vehicle + '<br> Tasks: Not assigned';
    //     }
    
            
            
    // }

// Script for map





    


        
       





