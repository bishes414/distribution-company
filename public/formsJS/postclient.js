"use strict"

var wsconn = new WebSocket("ws://127.0.0.1:3000");

var name, address, phone, latitude, longitude;

	
wsconn.onopen = function()
{
  console.log('Connection Opened!');
};


function sendData(){

var name = document.getElementById('name').value;
var address = document.getElementById('address').value;
var phone = document.getElementById('phone').value;
var latitude = document.getElementById('latitude').value;
var longitude = document.getElementById('longitude').value;

// sendData(name, address, phone, latitude, longitude);
    var jsonData = {
        	"origin": "client", 
        	"data": {
        		"name": name,
        		"address": address, 
        		"phone": phone, 
        		"latitude": latitude, 
        		"longitude": longitude
        	}
        };

    waitForSocketConnection(wsconn, function() {
        wsconn.send(JSON.stringify(jsonData));

	});
}

wsconn.onclose = function()
{

  console.log("Connection is closed...");
};

// console.log(name);

// function sendsData() {
//     var query = 'Bishes is the best';
//     waitForSocketConnection(wsconn, function() {
//         wsconn.send(query);
//     });
// }




/* Check if socket is connected */
var waitTimeOut = 0;

function waitForSocketConnection(socket, callback) {

    setTimeout(
        function() {
            waitTimeOut = waitTimeOut + 1;
            if (waitTimeOut === parseInt(3)) {
                waitTimeOut = 0;
                return;
            }

            if (socket.readyState === 1) {
                // console.log("Connection is made");
                if (callback != null) {
                    callback();
                }
                return;
            } else {
                console.log("wait for connection...")
                waitForSocketConnection(socket, callback);
            }

        }, 1000); // wait 1 second for the connection...
}
