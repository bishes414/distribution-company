"use strict"

var wsconn = new WebSocket("ws://127.0.0.1:3000");

var custid, product, quantity, date, timenum, meridian;

// dropdown.onclick = fetchData(); handled from html file 


	
wsconn.onopen = function()
{
  console.log('Connection Opened!');
};

wsconn.onmessage = function(evt){
    console.log(evt.data);
    var message = evt.data; 
    console.log(message);
    var msg = JSON.parse(message);

    if(msg.table == 'list')
    var vanlist = document.getElementById("vehicleid");



    for(var i = 0; i < msg.data.length; i++) {
        var opt = msg.data[i];
        console.log(msg.data[i]);
        var nl = document.createElement("option");

            nl.textContent = opt;
            nl.value = opt;
            vanlist.appendChild(nl);    
        
    }
}





function fetchData(){
    var jsonData = {
        "origin": "dropdown",
        "data": "blank"
    }
     waitForSocketConnection(wsconn, function() {
        wsconn.send(JSON.stringify(jsonData));
    });
}

function sendData(){

var custid = document.getElementById('custid').value;
var product = document.getElementById('product').value;
var quantity = document.getElementById('quantity').value;
var date = document.getElementById('date').value;
var timenum = document.getElementById('timenum').value;
var meridian = document.getElementById('meridian').value;
var time = timenum + meridian; 
var vehicleid = document.getElementById('vehicleid').value;
var status = document.getElementById('status').value; 


    var jsonData = {
        "origin": "job", 
        "data": {
            "custid": custid,
            "product": product, 
            "quantity": quantity,
            "date": date, 
            "time": time, 
            "vehicleid": vehicleid, 
            "status": status
        }
    }

    waitForSocketConnection(wsconn, function() {
        wsconn.send(JSON.stringify(jsonData));


	});
}



wsconn.onclose = function()
{
  // websocket is closed.
  console.log("Connection is closed...");
};

/* Check if socket is connected */
var waitTimeOut = 0;

function waitForSocketConnection(socket, callback) {

    setTimeout(
        function() {
            waitTimeOut = waitTimeOut + 1;
            if (waitTimeOut === parseInt(3)) {
                waitTimeOut = 0;
                return;
            }

            if (socket.readyState === 1) {
                // console.log("Connection is made");
                if (callback != null) {
                    callback();
                }
                return;
            } else {
                console.log("wait for connection...")
                waitForSocketConnection(socket, callback);
            }

        }, 1000); // wait 1 second for the connection...
}

