"use strict"

var wsconn = new WebSocket("ws://127.0.0.1:3000");

var vehicleid, drivername, status;

	
wsconn.onopen = function()
{
  console.log('Connection Opened!');
};



function sendData(){

var vehicleid = document.getElementById('vehicleid').value;
var drivername = document.getElementById('drivername').value;
var status = document.getElementById('status').value;

// sendData(name, address, phone, latitude, longitude);
	var jsonData = {
        	"origin": "vehicle", 
        	"data": {
        		"vehicleid": vehicleid,
        		"drivername": drivername, 
        		"status": status
        	}
        };

    waitForSocketConnection(wsconn, function() {
        wsconn.send(JSON.stringify(jsonData));


	});
} 



wsconn.onclose = function()
{
  // websocket is closed.
  console.log("Connection is closed...");
};






/* Check if socket is connected */
var waitTimeOut = 0;

function waitForSocketConnection(socket, callback) {

    setTimeout(
        function() {
            waitTimeOut = waitTimeOut + 1;
            if (waitTimeOut === parseInt(3)) {
                waitTimeOut = 0;
                return;
            }

            if (socket.readyState === 1) {
                // console.log("Connection is made");
                if (callback != null) {
                    callback();
                }
                return;
            } else {
                console.log("wait for connection...")
                waitForSocketConnection(socket, callback);
            }

        }, 1000); // wait 1 second for the connection...
}
