"use strict"



var ws = new WebSocket("ws://127.0.0.1:3000");


// body.onload = sendData();
function sendData() {
    console.log('HTML body loaded.');
    var query = "Interface is loaded."

    waitForSocketConnection(ws, function() {
        ws.send(query);
    });
}



ws.onmessage = function (evt){

  if(evt.data) {
//     var message = evt.data;
//     // var reply = messageHandler[message.cmd](message.msg);
    var message = evt.data;
    // message = JSON.parse(message);
    console.log(message);

    // console.log(typeof(message)); // string
    var msg = message.split(','); // array 
    var len = msg.length;
    // console.log(msg[1]);
    

    var namelist = document.getElementById("namelist"); 
    var tasklist = document.getElementById("tasklist");
    // namelist.onclick = sendData();
    // tasklist.onclick = sendData();
    // var vehicles = document.getElementById("vanlist");

    for(var i = 0; i < len; i++) {
        var opt = msg[i];
        console.log(msg[i]);
        var nl = document.createElement("option");

        if(opt.length >= 4){
            nl.textContent = opt;
            nl.value = opt;
            namelist.appendChild(nl);    
        }
        else if(opt.length < 4){
            nl.textContent = opt;
            nl.value = opt;
            tasklist.appendChild(nl);
        }
        
    }

  }
};







/* Check if socket is connected */
var waitTimeOut = 0;

function waitForSocketConnection(socket, callback) {

    setTimeout(
        function() {
            waitTimeOut = waitTimeOut + 1;
            if (waitTimeOut === parseInt(3)) {
                waitTimeOut = 0;
                return;
            }

            if (socket.readyState === 1) {
                // console.log("Connection is made");
                if (callback != null) {
                    callback();
                }
                return;
            } else {
                console.log("wait for connection...")
                waitForSocketConnection(socket, callback);
            }

        }, 1000); // wait 1 second for the connection...
}

